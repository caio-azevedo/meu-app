# Fazer um código para checar se um número digitado pelo o usuário é positivo, negativo ou zero.

numero = float(input("Digite um número: "))
if numero<0:
    condicao = "negativo"
elif numero>0:
    condicao = "positivo"
else:
    condicao = "zero"

print("O número digitado é {}.".format(condicao))

# Fazer um código para checar se um número digitado pelo o usuário é par ou ímpar.

numero = float(input("Digite um número: "))
resultado = numero % 2

if resultado == 1:
    condicao = "ÍMPAR"
else:
    condicao = "PAR"

print("O número digitado é {}.".format(condicao))

# Faça um código que pergunte o quanto se ganha por hora e o total de horas trabalhadas por mês.
# Calcule e mostre o total do seu salário no mês.

renda = float(input('Digite o valor ganho por hora: '))
horas = float(input('Digite o total de horas trabalhadas por mês: '))
salario = renda*horas
print('O salário total a ser recebido no mês é de R${:.2f}.' .format(salario))

# Faça um código para loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada.
# Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18
# litros, que custam R\$ 80,00 cada. Informe ao usuário a quantidade de latas de tinta a serem compradas e o preço total.
import math
area = float(input('Qual o tamanho da área em metros quadrados a ser pintada? '))
litros = area /3
latas = math.ceil(litros/18)
preco = latas*80
print('Você deve comprar {:.0f} lata(s) de tinta, totalizando {:.2f} reais.' .format(latas, preco))

# Faça outro programa para loja de tintas. O programa deverá pedir o tamanho em metros quadrados da área a ser pintada.
# Considere que a cobertura da tinta é de 1 litro para cada 6 metros quadrados e que a tinta é vendida em latas de 18
# litros, que custam R\$ 80,00 ou em galões de 4 litros, que custam R\$ 25,00. Informe ao usuário as quantidades de
# tinta a serem compradas e os respectivos preços em 3 situações:
# a) comprar apenas latas de 18 litros;
# b) comprar apenas galões de 4 litros;
# c) mistrurar latas e galões.

# a)
import math
area = float(input('Qual o tamanho da área em metros quadrados a ser pintada? '))
litros = area /6
latas = math.ceil(litros/18)
preco1 = latas*80
print('Você deve comprar {:.0f} lata(s) de 18 litros de tinta, totalizando {:.2f} reais.' .format(latas, preco1))

# b)

galoes = math.ceil(litros/4)
preco2 = galoes*25
print('Você deve comprar {:.0f} galões de 4 litros de tinta, totalizando {:.2f} reais.' .format(galoes, preco2))

# c)

x = litros%18

if x<=12:
      latas_1 = litros//18
else:
      latas_1 = math.ceil(litros/18)

litros_1 = litros - latas_1*18

if litros_1>0:
      galoes_1 = math.ceil(litros_1/4)
else:
      galoes_1 = 0

preco3 = latas_1*80 + galoes_1*25

print('Você deve comprar {:.0f} lata(s) de 18 litros de tinta e {:.0f} galões de 4 litros de tinta, totalizando {:.2f} reais.'
      .format(latas_1, galoes_1, preco3))

# Faça um programa de matemática financeira, para simular o valor de uma prestação do veículo, com as seguintes entradas:
# i) valor do produto;
# ii) entrada em dinheiro;
# iii) número de prestações a.m. para pagamento do saldo devedor;
# iv) taxa de juros a.m.
# Para sua realização, assuma um regime de juros composto.

valor = float(input('Digite o valor do veículo a ser comprado: '))
entrada = float(input('Digite o valor da entrada: '))
t = int(input('Digite o número de prestações mensais para a quitação do veículo: '))
taxa = float(input('Digite a taxa de juros de mensal da operação (em %): '))
i = taxa/100
pv = valor - entrada
cf = i/(1 - 1/(1 + i)**t)
pmt = pv*cf

print('O valor da parcela mensal para a aquisição do veículo é de R${:.2f} em {} prestações. \nConsiderando o valor do veículo de R${:.2f}'
      ', com entrada de R${:.2f} e taxa de juros mensal de {:.1f} %.' .format(pmt, t, valor, entrada, taxa))
